import sys

from fno_strategy import strategy

class Model(object):
    # version introduced in version 1 :)
    # tableName added in version 2
    CUR_VERSION=2
    def __init__(self):
        self._strategy_list = []
        self.version = 1
        self.dbFile = None
        self.startMonth = 'Jan'
        self.startYear = 2001
        self.endMonth = 'Jan'
        self.endYear = 2010
        self.tableName = "NIFTY"
        
    def __getstate__(self):
        state = self.__dict__.copy()
        state['version'] = self.CUR_VERSION
        return state
    
    def __setstate__(self, state):
        # we added version and _analysis_in_progress later in version 1
        if 'version' not in state:
            state['version'] = 1
            self.dbFile = None
            self.startMonth = 'Jan'
            self.startYear = 2001
            self.endMonth = 'Jan'
            self.endYear = 2010
                
        self.__dict__.update(state)

        if self.version == 1:
            self.tableName = "NIFTY"
                
    def get_strategy_list(self):
        return self._strategy_list
    
    def set_strategy_list(self, strategy_list):
        self._strategy_list = strategy_list
        
    def add_strategy(self, strategy):
        self._strategy_list.append(strategy)
        
    def update_strategy(self, index, strategy):
        self._strategy_list[index] = strategy

        