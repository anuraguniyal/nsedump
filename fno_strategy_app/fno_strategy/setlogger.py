import sys
import traceback
import logging

import wx

class MockupPyLog(wx.PyLog):
    def __init__(self):
        wx.PyLog.__init__(self)

    def DoLogString(self, message, timeStamp):
        print "wxpython message:"+message
      
def myExceptHook(type, value, tb):
    print "uncaught: %s: %s"%(type, value)
    print "\n".join(traceback.format_tb(tb))
    
def setlogger():
    sys.excepthook = myExceptHook
    wx.Log_SetActiveTarget(MockupPyLog())
    
    logger = logging.getLogger()
    logger.setLevel(logging.DEBUG)
    
    fileHandler = logging.FileHandler("fnostrategy.log", mode='w')
    fileHandler.setLevel(logging.DEBUG)

    streamHandler = logging.StreamHandler()
    streamHandler.setLevel(logging.INFO)
    
    # create formatter and add it to the handlers
    formatter = logging.Formatter("[%(asctime)s %(levelname)s] %(message)s ")
    streamHandler.setFormatter(formatter)
    fileHandler.setFormatter(formatter)
    
    logger.addHandler(streamHandler)
    logger.addHandler(fileHandler)
    