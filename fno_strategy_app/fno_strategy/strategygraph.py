import wx
import wx.lib.plot as plot

class StrategyGraph(wx.Frame):
    def __init__(self, parent, model, strategy_data_list):
        wx.Frame.__init__(self, parent, size=(500,400))
        self.SetTitle("Strategy Results")
        
        self.model = model
        self.strategy_data_list = strategy_data_list
        
        self.plotter = plotter = plot.PlotCanvas(self)
        plotter.SetEnableZoom(True)
        self.plotter.SetBackgroundColour((255,255,255))
   
        self._draw()
    
        # buttons

        self.btnPanel = wx.Panel(self)
        btnReset = wx.Button(self.btnPanel, label="Reset")
        btnReset.Bind(wx.EVT_BUTTON, self._onReset)
        btnToggleZoom = wx.Button(self.btnPanel, label="Toggle Zoom/Drag")
        btnToggleZoom.Bind(wx.EVT_BUTTON, self._onToggleZoom)
        
        sizer = wx.BoxSizer(wx.HORIZONTAL)
        sizer.Add(btnReset)
        sizer.Add(btnToggleZoom)
        self.btnPanel.SetSizerAndFit(sizer)
        
        self.Bind(wx.EVT_SIZE, self._onSize)
        
    def _onReset(self, event):
        self.plotter.Reset()
        
    def _onToggleZoom(self, event):
        enableDrag = self.plotter.GetEnableZoom()
        self.plotter.SetEnableZoom(not enableDrag)
        self.plotter.SetEnableDrag(enableDrag)

    def _draw(self):
        colours = [(255,0,0), (0,255,0), (0,0,255), (255,255,0), (255, 0, 255), (0,255,255), (0,0,0)]
        lines = []
        for strategy_index, strategy_data in enumerate(self.strategy_data_list):
            data = []
            for i, (day, value) in enumerate(strategy_data):
                day = day.replace("-","")
                day = int(day)
                data.append((day, float(value)))
                
            line = plot.PolyLine(data, colour=colours[strategy_index], width=1)
            lines.append(line)
            
        gc = plot.PlotGraphics(lines, 'Line/Marker Graph', 'x axis', 'y axis')
        self.plotter.Draw(gc)
        
    def _onSize(self, event):
        w, h = self.GetClientSizeTuple()
        toolH = self.btnPanel.GetSize()[1]
        h -= toolH
        self.plotter.SetPosition((0,toolH))
        self.plotter.SetSize((w,h))
        #self._draw()